let img=[];
let pos=255, g=255, i=0;

function preload() {
   img[0] = loadImage('data/1.jpg');
   img[1] = loadImage('data/2.jpg');
   img[2] = loadImage('data/3.jpg');
   img[3] = loadImage('data/4.jpg');
}

function setup() {
  createCanvas(1080, 720);
 }

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function draw(){
  background(253);
  tint(255, g);
  image(img[i],0,0);
  img[i].resize(windowWidth, windowHeight); 
  
}

function keyPressed() {
  if (keyCode === LEFT_ARROW && i>0) {
    i = i - 1;
    g = 255;
      fullscreen();
  } else if (keyCode === RIGHT_ARROW && i<3) {
    i = i + 1;
    g = 255;
      fullscreen();
  }
    if (keyCode === TAB) {
    let fs = fullscreen();
    fullscreen(!fs);}
}

function mouseWheel() {
    if (event.delta > 0) {
    g = g + 30;
  } else {
    g = g - 30;
  }
 }



  
