let song=[];
let pos=255, i=0;

function preload() {
   song[0] = loadSound('data/siren.mp3');
   song[1] = loadSound('data/drinking.mp3');
   song[2] = loadSound('data/Moiroloifi.mp3');
  }

function setup() {
  createCanvas(1080, 720);
   
 }

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function draw(){
  background(255);
    // Set the volume to a range between 0 and 1.0
  let volume = map(mouseX, 0, width, 0, 1);
  volume = constrain(volume, 0, 1);
  song[i].amp(volume);

  // Set the rate to a range between 0.1 and 4
  // Changing the rate alters the pitch
  let speed = map(mouseY, 0.1, height, 0, 2);
  speed = constrain(speed, 0.01, 4);
  song[i].rate(speed);

  // Draw some circles to show what is going on
  stroke(0);
  fill(51, 0);
  ellipse(mouseX, 100, 48, 48);
  stroke(0);
  fill(51, 0);
  ellipse(100, mouseY, 48, 48);
}

function keyPressed() {
  if (keyCode === LEFT_ARROW && i>0) {
    i = i - 1;
    g = 255;
    song[i+1].stop()
    song[i].play()
  } else if (keyCode === RIGHT_ARROW && i<2) {
    i = i + 1;
    g = 255;
    song[i-1].stop()
    song[i].play()
  }
    if (keyCode === TAB) {
    let fs = fullscreen();
    fullscreen(!fs);}
}

function mousePressed() {
  
  if (song[2].isPlaying()){
    song[0].stop()
    song[1].play();
  } else {
    song[0].play();
  }
  
}

 


 
