let img=[];
let song=[];
let vid=[];
let vidPlay = false;
let i=0;
let j=4;

function preload() {
   img[1] = loadImage('data/1.jpg');
   img[2] = loadImage('data/2.jpg');
   img[3] = loadImage('data/3.jpg');
   img[0] = loadImage('data/0.jpg');
   song[0] = loadSound('data/aoristos.mp3');
   song[1] = loadSound('data/drinking.mp3');
   song[2] = loadSound('data/void.mp3');
   vid[0] = createVideo('data/gala1.mp4');
   vid[1] = createVideo('data/Moiroloi2.mp4');
   vid[2] = createVideo('data/Moiroloi3.mp4');
}

function setup() {
  createCanvas(1080, 720);
  vid[0].hide();
  vid[1].hide();
  vid[2].hide();
 }

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function draw(){
  background(253);
  image(img[i],0,0);
  img[i].resize(windowWidth, windowHeight);
  if (vidPlay==true){
    vid[j].size(windowWidth, windowHeight);
    image(vid[j],0,0,width, height);
  }
  }



function mousePressed() {
    let d1 = int(dist(700,670,mouseX,mouseY))
    let d2 = int(dist(750,370,mouseX,mouseY))
    let d3 = int(dist(260,750,mouseX,mouseY))
    let d4 = int(dist(750,280,mouseX,mouseY))
    let d5 = int(dist(700,750,mouseX,mouseY))
    let d6 = int(dist(1150,350,mouseX,mouseY))
    let d7 = int(dist(720,50,mouseX,mouseY))
    let d8 = int(dist(300,200,mouseX,mouseY))
    let d9 = int(dist(450,450,mouseX,mouseY))
    if(d1<50){
      i = 1
    }else if (d2<50) {
      i = 2   
    }else if (d3<150) {
      i = 3    
    }else if (d4<30) {
      song[2].play()
    }else if (d5<100) {
      song[0].play() 
    }else if (d6<150) {
      song[1].play() 
    }else if (d7<100) {
      vidPlay=true;
      j = 0;
      vid[j].loop();     
    }else if (d8<100) {
      vidPlay=true;
      j = 1;
      vid[j].loop(); 
    }else if (d9<100) {
      vidPlay=true;
      j = 2;
      vid[j].loop();  
    }
 }

function keyPressed() {
   if (keyCode === TAB) {
    let fs = fullscreen();
    fullscreen(!fs);}
    if (keyCode === LEFT_ARROW) {
    vidPlay = false;
      i = 0;
      vid[0].hide();
        song[0].stop();
        vid[0].stop();
        song[1].stop();
        vid[1].stop();
        song[2].stop();
        vid[2].stop();
    }
}
