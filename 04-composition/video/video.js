let vid=[];
let pos=255, g=1, i=0;

function preload() {    
   vid[0] = createVideo('data/gala1.mp4');
   vid[1] = createVideo('data/Moiroloi2.mp4');
   vid[2] = createVideo('data/Moiroloi3.mp4');
  }

function setup() {
   createCanvas(windowWidth, windowHeight);
   vid[0].hide();
   vid[1].hide();
   vid[2].hide();
 }

function draw(){
  background(220);
  vid[i].size(windowWidth, windowHeight)
  image(vid[i],0,0,width, height);
  vid[i].speed(g);    
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  windowResized();
}

function keyPressed() {
  if (keyCode === LEFT_ARROW && i>0) {
    i = i - 1;
    g = 1;
    vid[i+1].stop()
    vid[i].loop()
  } else if (keyCode === RIGHT_ARROW && i<2) {
    i = i + 1;
    g = 1;
    vid[i-1].stop()
    vid[i].loop()
  }
    if (keyCode === TAB) {
    let fs = fullscreen();
    fullscreen(!fs);}
}

function mousePressed() {
    vid[0].loop();
  }

function mouseWheel() {
    if (event.delta > 0 && g<8) {
    g = g + 0.1;
  } else if (event.delta < 0 && g>0.4){
    g = g - 0.1;
  }
 }
